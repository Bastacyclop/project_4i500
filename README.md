# ALGAV - Devoir de programmation: Tries

Language choisi: [Rust](https://www.rust-lang.org).

## Prérequis

- Rust, nous utilisons nightly 1.13 mais avons régulièrement vérifié que tout compile sur les dernières branches stable, beta et nightly
- graphviz pour certains affichages (commande shell `dot`)

## Générer la documentation

```sh
cargo doc --no-deps
# généré dans './target/doc/project/index.html'
```

## Compiler et Exectuer

```sh
# génère des fichiers .gv et .png montrant les structures dans le dossier 'graphviz'
# ne pas donner d'argument utiliseras la phrase 'exemple de base'
cargo run --release --bin graphviz [-- file]
# effectue des mesures sur les structures contenant les mots des fichiers du dossier 'shakespeare'
cargo run --release --bin shakespeare
# effectue une comparaison entre la fusion de patricia tries et une série d'insertion
cargo run --release --bin patricia_merge
# effectue quelques assertions sur nos fonctions de conversions et de fusion
cargo run --release --bin checkspeare
# mesure l'impact du réequilibrage d'hybrid trie sur l'oeuvre complète de shakespeare
cargo run --release --bin full_checkspeare
```

-------------------------------------------------------------------------------

Thomas Koehler & Andrew Hatoum
