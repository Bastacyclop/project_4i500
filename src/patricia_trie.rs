use std::{mem, io, fmt, str};
use std::cmp::Ordering::*;
use std::path::Path;
use Dictionnary;

#[derive(Debug, PartialEq)]
pub struct PatriciaTrie {
    pub root: Node
}

#[derive(PartialEq, Clone)]
pub struct Edge {
    pub label: Vec<u8>,
    pub target: Node,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Node {
    pub edges: Vec<Edge>,
    pub is_word: bool
}

impl fmt::Debug for Edge {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Edge {{ label: {:?}, target: {:?} }}", str::from_utf8(&self.label).unwrap(), self.target)
    }
}

fn is_prefix(p: &[u8], s: &[u8]) -> bool {
    if p.len() > s.len() {
        false
    } else {
        p == &s[..p.len()]
    }
}

fn common_prefix_len(a: &[u8], b: &[u8]) -> Option<usize> {
    let mut found = None;
    for (pos, (a, b)) in a.iter().zip(b.iter()).enumerate() {
        if a == b { found = Some(pos) } else { break; }
    }
    found.map(|pos| pos + 1)
}

// hypothesis: no empty slices
fn has_common_prefix(a: &[u8], b: &[u8]) -> bool {
    a[0] == b[0]
}

impl Edge {
    fn with_edges(label: Vec<u8>, edges: Vec<Edge>) -> Edge {
        Edge {
            target: Node { edges: edges, is_word: false },
            label: label
        }
    }

    fn cut(&mut self, size: usize) {
        let rest = self.label[size..].to_owned();
        self.label.truncate(size);
        let mut next = Edge::with_edges(rest, mem::replace(&mut self.target.edges, Vec::new()));
        next.target.is_word = self.target.is_word;
        self.target.is_word = false;
        self.target.edges.push(next);
    }

    fn try_blend(&mut self) -> bool {
        if self.target.edges.len() == 1 && !self.target.is_word {
            let blend = self.target.edges.pop().unwrap();
            self.label.extend_from_slice(&blend.label);
            self.target = blend.target;
            true
        } else {
            false
        }
    }

    fn should_drop(&self) -> bool {
        self.target.edges.is_empty() && !self.target.is_word
    }
}

impl Node {
    fn is_leaf(&self) -> bool {
        self.edges.is_empty()
    }

    fn insert(&mut self, word: &[u8]) -> bool {
        if word.is_empty() {
            let inserted = !self.is_word;
            self.is_word = true;
            inserted
        } else {
            for mut e in &mut self.edges {
                if is_prefix(&e.label, word) {
                    return e.target.insert(&word[e.label.len()..]);
                } else {
                    match common_prefix_len(word, &e.label) {
                        Some(len) => {
                            e.cut(len);
                            return e.target.insert(&word[len..]);
                        }
                        None => {}
                    }
                }
            }

            let mut edge = Edge::with_edges(word.to_owned(), Vec::new());
            edge.target.is_word = true;
            self.edges.push(edge);
            true
        }
    }

    fn contains(&self, word: &[u8]) -> bool {
        if word.is_empty() {
            self.is_word
        } else {
            for e in &self.edges {
                if is_prefix(&e.label, word) {
                    return e.target.contains(&word[e.label.len()..]);
                } else if has_common_prefix(word, &e.label) {
                    return false;
                }
            }

            false
        }
    }

    fn remove(&mut self, word: &[u8]) -> bool {
        if word.is_empty() {
            let removed = self.is_word;
            self.is_word = false;
            return removed;
        }

        let mut removed = false;
        let mut next = None;
        for (i, e) in self.edges.iter_mut().enumerate() {
            if is_prefix(&e.label, word) {
                removed = e.target.remove(&word[e.label.len()..]);
                next = Some(i);
                break;
            } else if has_common_prefix(word, &e.label) {
                return false;
            }
        }

        if let Some(i) = next {
            if !self.edges[i].try_blend() {
                if self.edges[i].should_drop() {
                    self.edges.swap_remove(i);
                }
            }
        }
        removed
    }

    fn word_count(&self) -> usize {
        self.edges.iter().fold(if self.is_word { 1 } else { 0 }, |c, e| c + e.target.word_count())
    }

    fn words(&self, v: &mut Vec<String>, w: &mut Vec<u8>) {
        if self.is_word {
            v.push(String::from_utf8(w.clone()).unwrap());
        }

        for e in &self.edges {
            let before = w.len();
            w.extend_from_slice(&e.label);
            e.target.words(v, w);
            w.truncate(before);
        }
    }

    fn prefix_count(&self, prefix: &[u8]) -> usize {
        if prefix.is_empty() {
            self.word_count()
        } else {
            for e in &self.edges {
                if is_prefix(&e.label, prefix) {
                    return e.target.prefix_count(&prefix[e.label.len()..]);
                } else if is_prefix(prefix, &e.label) {
                    return e.target.word_count();
                } else if has_common_prefix(prefix, &e.label) {
                    return 0;
                }
            }

            0
        }
    }

    fn leaf_count(&self) -> usize {
        self.edges.iter().fold(if self.is_leaf() { 1 } else { 0 }, |c, e| c + e.target.leaf_count())
    }

    // height in number of edges
    fn tree_height(&self) -> usize {
        self.edges.iter().map(|e| 1 + e.target.tree_height()).max().unwrap_or(0)
    }

    fn average_depth_acc(&self, depth: usize, depth_sum: &mut usize, word_count: &mut usize) {
        if self.is_word {
            *word_count += 1;
            *depth_sum += depth;
        }

        for e in &self.edges {
            e.target.average_depth_acc(depth + 1, depth_sum, word_count);
        }
    }

    fn average_width_acc(&self, depth: usize, mut width: usize, width_sum: &mut f32) {
        if self.is_word {
            *width_sum += width as f32 / depth as f32;
        }

        width += self.edges.len();
        for e in &self.edges {
            e.target.average_width_acc(depth + 1, width, width_sum);
        }
    }

    fn memory_usage(&self, used: &mut usize, alloc: &mut usize) {
        *used += self.edges.len() * mem::size_of::<Edge>();
        *alloc += self.edges.capacity() * mem::size_of::<Edge>();
        for e in &self.edges {
            *used += e.label.len();
            *alloc += e.label.capacity();
            e.target.memory_usage(used, alloc);
        }
    }

    fn graphviz<W: io::Write>(&self, w: &mut W, word: &mut String) {
        let shape_arg = if self.is_word {"shape=doublecircle"} else { "" };
        writeln!(w, "  \"{}\" [label=\"\", {}]", word, shape_arg).unwrap();

        for e in &self.edges {
            let before = word.len();
            write!(w, "  \"{}\" -> {{ ", word).unwrap();
            let l = str::from_utf8(&e.label).unwrap();
            word.push_str(&l);
            writeln!(w, "\"{}\" }} [label=\"{}\"]", word, l).unwrap();
            e.target.graphviz(w, word);
            word.truncate(before);
        }
    }
}

fn merge_edges(a: &mut Edge, b: &mut Edge) -> Edge {
    fn insert_edge(edges: &mut Vec<Edge>, mut edge: Edge) {
        for e in edges.iter_mut() {
            if has_common_prefix(&e.label, &edge.label) {
                let merge = merge_edges(e, &mut edge);
                mem::replace(e, merge);
                return;
            }
        }

        edges.push(edge);
    }

    let cp = common_prefix_len(&a.label, &b.label).unwrap();
    if (cp == a.label.len()) && (cp == b.label.len()) {
        Edge { label: a.label.clone(), target: merge(&mut a.target, &mut b.target) }
    } else if cp == a.label.len() {
        let mut e = a.clone();
        insert_edge(&mut e.target.edges, Edge { label: b.label[cp..].to_owned(),
                                                target: b.target.clone() });
        e
    } else if cp == b.label.len() {
        let mut e = b.clone();
        insert_edge(&mut e.target.edges, Edge { label: a.label[cp..].to_owned(),
                                                target: a.target.clone() });
        e
    } else {
        let left = Edge { label: a.label[cp..].to_owned(), target: a.target.clone() };
        let right = Edge { label: b.label[cp..].to_owned(), target: b.target.clone() };
        Edge::with_edges(a.label[..cp].to_owned(), vec![left, right])
    }
}

fn merge(a: &mut Node, b: &mut Node) -> Node {
    let mut r = Node { is_word: a.is_word || b.is_word, edges: Vec::new() };
    a.edges.sort_by(|a, b| a.label[0].cmp(&b.label[0]));
    b.edges.sort_by(|a, b| a.label[0].cmp(&b.label[0]));
    let mut ia = 0;
    let mut ib = 0;

    loop {
        match (ia < a.edges.len(), ib < b.edges.len()) {
            (true, true) => {
                let edge_a = &mut a.edges[ia];
                let edge_b = &mut b.edges[ib];
                match edge_a.label[0].cmp(&edge_b.label[0]) {
                    Less => {
                        r.edges.push(edge_a.clone());
                        ia += 1;
                    }
                    Greater => {
                        r.edges.push(edge_b.clone());
                        ib += 1;
                    }
                    Equal => {
                        r.edges.push(merge_edges(edge_a, edge_b));
                        ia += 1;
                        ib += 1;
                    }
                }
            }
            (true, false) => {
                r.edges.push(a.edges[ia].clone());
                ia += 1;
            }
            (false, true) => {
                r.edges.push(b.edges[ib].clone());
                ib += 1;
            }
            (false, false) => { break; }
        }
    }

    r
}

impl PatriciaTrie {
    pub fn merge(a: &mut Self, b: &mut Self) -> Self {
        PatriciaTrie { root: merge(&mut a.root, &mut b.root) }
    }
}

impl Dictionnary for PatriciaTrie {
    fn empty() -> Self {
        PatriciaTrie {
            root: Node { edges: Vec::new(), is_word: false }
        }
    }

    fn graphviz<P: AsRef<Path>>(&self, path: P) {
        use std::fs::File;
        use std::io::Write;
        let f = File::create(path).unwrap();
        let mut w = io::BufWriter::new(f);
        writeln!(w, "digraph patricia {{").unwrap();
        writeln!(w, "  node [shape=circle]").unwrap();
        self.root.graphviz(&mut w, &mut String::new());
        writeln!(w, "}}").unwrap();
    }

    fn memory_usage(&self) -> (usize, usize) {
        let mut used = mem::size_of::<Self>();
        let mut alloc = mem::size_of::<Self>();
        self.root.memory_usage(&mut used, &mut alloc);
        (used, alloc)
    }

    fn insert(&mut self, word: &str) -> bool {
        self.root.insert(word.as_bytes())
    }

    fn contains(&self, word: &str) -> bool {
        self.root.contains(word.as_bytes())
    }

    fn remove(&mut self, word: &str) -> bool {
        self.root.remove(word.as_bytes())
    }

    fn word_count(&self) -> usize {
        self.root.word_count()
    }

    fn ordered_words(&self, v: &mut Vec<String>) {
        self.root.words(v, &mut Vec::new());
        v.sort();
    }

    fn prefix_count(&self, prefix: &str) -> usize {
        self.root.prefix_count(prefix.as_bytes())
    }

    fn nil_count(&self) -> usize {
        // in this implementation, only leafs have nils
        self.root.leaf_count()
    }

    fn tree_height(&self) -> usize {
        self.root.tree_height()
    }

    fn average_depth(&self) -> f32 {
        let mut ds = 0;
        let mut wc = 0;
        self.root.average_depth_acc(0, &mut ds, &mut wc);
        let mut ws = 0.0;
        self.root.average_width_acc(0, 0, &mut ws);
        println!("patricia average width per node per word: {}",
                 (ws / wc as f32).round());
        (ds as f32 / wc as f32)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    macro_rules! dict_tests {
        ($($test:ident),*) => {
            $(
                #[test]
                fn $test() { ::dictionnary::test::$test::<PatriciaTrie>() }
            )*
        }
    }

    dict_tests! {
        insert,
        remove,
        word_count,
        ordered_words,
        prefix_count
    }

    #[test]
    fn test_is_prefix() {
        use super::is_prefix;

        let s = b"Hello";
        assert_eq!(is_prefix(s, b"Hel"), false);
        assert_eq!(is_prefix(s, b"Hello"), true);
        assert_eq!(is_prefix(s, b"HelloAloa"), true);
    }

    #[test]
    fn test_common_prefix() {
        use super::common_prefix_len;

        let s = b"Hello";
        assert_eq!(common_prefix_len(s, b"Hel"), Some(3));
        assert_eq!(common_prefix_len(s, b"Hello"), Some(5));
        assert_eq!(common_prefix_len(s, b"HelloAloa"), Some(5));
        assert_eq!(common_prefix_len(s, b"AloaHello"), None);
    }

    #[test]
    fn test_edge_cut() {
        use super::Edge;

        let mut e = Edge::with_edges((b"Hello").to_vec(), Vec::new());
        e.cut(2);
        assert_eq!(&e.label, b"He");
        assert_eq!(&e.target.edges[0].label, b"llo");
        e.cut(1);
        assert_eq!(&e.label, b"H");
        assert_eq!(&e.target.edges[0].label, b"e");
    }

    #[test]
    fn test_merge() {
        use Dictionnary;
        use super::PatriciaTrie as PT;

        let a_words = &["thomas", "tomato", "tomb", "raider", "calcul", "product", "apply"];
        let b_words = &["toto", "drop", "ncube", "polynomial", "question", "do it"];
        let mut a = PT::empty();
        let mut b = PT::empty();

        for w in a_words { a.insert(w); }
        for w in b_words { b.insert(w); }

        let m = PT::merge(&mut a, &mut b);
        for w in a_words.iter().chain(b_words.iter()) {
            assert!(m.contains(w));
        }
        assert_eq!(m.word_count(), a_words.len() + b_words.len());
    }
}
