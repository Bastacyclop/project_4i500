#![cfg_attr(all(feature = "unstable", test), feature(test))]

#[cfg(all(feature = "unstable", test))]
extern crate test;

pub mod dictionnary;
pub mod hybrid_trie;
pub mod patricia_trie;
pub mod convert;

pub use dictionnary::Dictionnary;
pub use hybrid_trie::HybridTrie;
pub use patricia_trie::PatriciaTrie;
