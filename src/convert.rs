use patricia_trie::{self as pat, PatriciaTrie};
use hybrid_trie::{self as hyb, HybridTrie};

impl<'a> From<&'a HybridTrie> for PatriciaTrie {
    fn from(hyb: &'a HybridTrie) -> PatriciaTrie {
        PatriciaTrie {
            root: pat::Node {
                edges: hyb_to_pat(&hyb.root),
                is_word: false,
            }
        }
    }
}

fn hyb_to_pat(e: &hyb::Edge) -> Vec<pat::Edge> {
    fn build_edges(n: &hyb::Node, edges: &mut Vec<pat::Edge>) {
        build_edge(n, Vec::new(), edges);
        n.less.as_ref().map(|l| build_edges(l, edges));
        n.greater.as_ref().map(|g| build_edges(g, edges));
    }

    fn build_edge(n: &hyb::Node, mut label: Vec<u8>, edges: &mut Vec<pat::Edge>) {
        if !label.is_empty() && (n.less.is_some() || n.greater.is_some()) {
            let mut es = Vec::new();
            build_edges(n, &mut es);
            edges.push(pat::Edge {
                label: label,
                target: pat::Node { edges: es, is_word: false }
            });
            return;
        }

        label.push(n.digit);
        if n.is_word {
            edges.push(pat::Edge {
                label: label,
                target: pat::Node { edges: hyb_to_pat(&n.equal), is_word: true }
            });
        } else {
            n.equal.as_ref().map(|e| build_edge(e, label, edges));
        }
    }

    let mut edges = Vec::new();
    e.as_ref().map(|n| build_edges(n, &mut edges));
    edges
}

impl<'a> From<&'a mut PatriciaTrie> for HybridTrie {
    fn from(pat: &'a mut PatriciaTrie) -> HybridTrie {
        HybridTrie {
            root: pat_to_hyb(&mut pat.root.edges[..]),
        }
    }
}

fn pat_to_hyb(edges: &mut [pat::Edge]) -> hyb::Edge {
    fn build_node(equal: &mut pat::Edge,
                  less: &mut [pat::Edge],
                  greater: &mut [pat::Edge]) -> Box<hyb::Node> {
        let mut n = build_equal(&equal.label, &mut equal.target);
        n.less = pat_to_hyb(less);
        n.greater = pat_to_hyb(greater);
        n.update_height();
        n
    }

    fn build_equal(label: &[u8], target: &mut pat::Node) -> Box<hyb::Node> {
        let (&digit, rest) = label.split_first().unwrap();
        let mut n = Box::new(hyb::Node::new(digit));
        if rest.is_empty() {
            n.is_word = target.is_word;
            n.equal = pat_to_hyb(&mut target.edges[..]);
        } else {
            n.equal = Some(build_equal(rest, target));
        }
        n.update_height();

        n
    }

    if edges.is_empty() {
        None
    } else {
        edges.sort_by(|a, b| a.label[0].cmp(&b.label[0]));
        let mid = edges.len() / 2;
        let (less, rest) = edges.split_at_mut(mid);
        let (equal, greater) = rest.split_first_mut().unwrap();
        Some(build_node(equal, less, greater))
    }
}

#[cfg(test)]
pub mod test {
    use {Dictionnary, PatriciaTrie, HybridTrie};
    const WORDS: &'static [&'static str] = &["they", "could", "convert", "them", "back", "and", "forth"];

    #[test]
    pub fn hybrid_to_patricia() {
        let mut hyb = HybridTrie::empty();
        for w in WORDS { hyb.insert(w); }
        let pat = PatriciaTrie::from(&hyb);
        for w in WORDS { assert!(pat.contains(w)); }
        assert!(hyb.word_count() == pat.word_count());
    }

    #[test]
    pub fn patricia_to_hybrid() {
        let mut pat = PatriciaTrie::empty();
        for w in WORDS { pat.insert(w); }
        let hyb = HybridTrie::from(&mut pat);
        for w in WORDS { assert!(hyb.contains(w)); }
        assert!(hyb.word_count() == pat.word_count());
    }
}
