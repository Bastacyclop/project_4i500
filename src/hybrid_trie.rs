use std::cmp;
use std::cmp::Ordering::*;
use std::{io, mem};
use std::path::Path;
use Dictionnary;

#[derive(Debug)]
pub struct HybridTrie {
    pub root: Edge,
}

#[derive(Debug, Clone)]
pub struct Node {
    pub digit: u8,
    pub is_word: bool,
    pub height: Height,
    pub less: Edge,
    pub equal: Edge,
    pub greater: Edge,
}

pub type Edge = Option<Box<Node>>;
pub type Height = u16;

impl Node {
    pub fn new(digit: u8) -> Node {
        Node {
            digit: digit,
            is_word: false,
            height: 1,
            less: None,
            equal: None,
            greater: None,
        }
    }

    pub fn update_height(&mut self) {
        self.height = 1 + cmp::max(height(&self.less),
                                   cmp::max(height(&self.equal),
                                            height(&self.greater)));
    }

    fn leans_left(&self, threshold: Height) -> bool {
        let right = cmp::max(height(&self.equal), height(&self.greater));
        height(&self.less) >= right + threshold
    }

    fn leans_right(&self, threshold: Height) -> bool {
        let left = cmp::max(height(&self.less), height(&self.equal));
        height(&self.greater) >= left + threshold
    }
}

fn height(e: &Edge) -> Height {
    e.as_ref().map(|n| n.height).unwrap_or(0)
}

fn rotate_left(mut p: Box<Node>) -> Box<Node> {
    let mut q = p.greater.take().expect("no right child");
    let b = q.less.take();
    p.greater = b;
    p.update_height();
    q.less = Some(p);
    q.update_height();
    q
}

fn rotate_right(mut q: Box<Node>) -> Box<Node> {
    let mut p = q.less.take().expect("no left child");
    let b = p.greater.take();
    q.less = b;
    q.update_height();
    p.greater = Some(q);
    p
}

fn may_shift_left(mut n: Box<Node>) -> Box<Node> {
    if n.leans_right(2) {
        if let Some(r) = n.greater.take() {
            n.greater = if r.leans_left(1) {
                Some(rotate_right(r))
            } else {
                Some(r)
            };
        }
        rotate_left(n)
    } else {
        n
    }
}

fn may_shift_right(mut n: Box<Node>) -> Box<Node> {
    if n.leans_left(2) {
        if let Some(l) = n.less.take() {
            n.less = if l.leans_right(1) {
                Some(rotate_left(l))
            } else {
                Some(l)
            }
        }
        rotate_right(n)
    } else {
        n
    }
}

fn insert_no_rebalance(digit: u8, rest: &[u8], e: Edge, inserted: &mut bool) -> Edge {
    macro_rules! insert {
        ($d:ident, $r:ident, $e:expr) => {
            $e = insert($d, $r, $e.take(), inserted)
        }
    }

    match e {
        None => {
            let mut n = Box::new(Node::new(digit));
            match rest.split_first() {
                None => {
                    n.is_word = true;
                    *inserted = true;
                }
                Some((&digit, rest)) => {
                    insert!(digit, rest, n.equal);
                    if *inserted { n.update_height(); }
                }
            }
            Some(n)
        }
        Some(mut n) => {
            match digit.cmp(&n.digit) {
                Less => {
                    insert!(digit, rest, n.less);
                    if *inserted { n.update_height(); }
                }
                Greater => {
                    insert!(digit, rest, n.greater);
                    if *inserted { n.update_height(); }
                }
                Equal => match rest.split_first() {
                    None => {
                        *inserted = !n.is_word;
                        n.is_word = true;
                    }
                    Some((&digit, rest)) => {
                        insert!(digit, rest, n.equal);
                        if *inserted { n.update_height(); }
                    }
                }
            };
            Some(n)
        }
    }
}

fn insert(digit: u8, rest: &[u8], e: Edge, inserted: &mut bool) -> Edge {
    macro_rules! insert {
        ($d:ident, $r:ident, $e:expr) => {
            $e = insert($d, $r, $e.take(), inserted)
        }
    }

    match e {
        None => {
            let mut n = Box::new(Node::new(digit));
            match rest.split_first() {
                None => {
                    n.is_word = true;
                    *inserted = true;
                }
                Some((&digit, rest)) => {
                    insert!(digit, rest, n.equal);
                    if *inserted { n.update_height(); }
                }
            }
            Some(n)
        }
        Some(mut n) => {
            match digit.cmp(&n.digit) {
                Less => {
                    insert!(digit, rest, n.less);
                    if *inserted {
                        n.update_height();
                        return Some(may_shift_right(n));
                    }
                }
                Greater => {
                    insert!(digit, rest, n.greater);
                    if *inserted {
                        n.update_height();
                        return Some(may_shift_left(n));
                    }
                }
                Equal => match rest.split_first() {
                    None => {
                        *inserted = !n.is_word;
                        n.is_word = true;
                    }
                    Some((&digit, rest)) => {
                        insert!(digit, rest, n.equal);
                        if *inserted { n.update_height(); }
                    }
                }
            };
            Some(n)
        }
    }
}

fn contains(digit: u8, rest: &[u8], e: &Edge) -> bool {
    match e {
        &None => false,
        &Some(ref n) => match digit.cmp(&n.digit) {
            Less => contains(digit, rest, &n.less),
            Greater => contains(digit, rest, &n.greater),
            Equal => match rest.split_first() {
                None => n.is_word,
                Some((&digit, rest)) => contains(digit, rest, &n.equal),
            }
        }
    }
}

fn remove(digit: u8, rest: &[u8], e: Edge, removed: &mut bool) -> Edge {
    macro_rules! remove {
        ($d:ident, $r:ident, $e:expr) => {
            $e = remove($d, $r, $e.take(), removed)
        }
    }

    match e {
        None => None,
        Some(mut n) => {
            match digit.cmp(&n.digit) {
                Less => remove!(digit, rest, n.less),
                Greater => remove!(digit, rest, n.greater),
                Equal => match rest.split_first() {
                    None => {
                        *removed = n.is_word;
                        n.is_word = false;
                    }
                    Some((&digit, rest)) => remove!(digit, rest, n.equal),
                }
            };

            if *removed {
                if !n.is_word {
                    match (n.less.take(), n.equal.take(), n.greater.take()) {
                        (c, None, None) | (None, None, c) => return c,
                        (Some(l), None, Some(g)) => {
                            let (l, mut lgtr) = remove_greater(l);
                            lgtr.less = l;
                            lgtr.greater = Some(g);
                            lgtr.update_height();
                            return Some(lgtr);
                        }
                        (l, e, g) => {
                            n.less = l;
                            n.equal = e;
                            n.greater = g;
                        }
                    }
                }
                n.update_height();
            }
            Some(n)
        }
    }
}

fn remove_greater(mut n: Box<Node>) -> (Edge, Box<Node>) {
    if let Some(g) = n.greater.take() {
        let (g, gtr) = remove_greater(g);
        // the remaining subtrie has to stay valid
        n.greater = g;
        n.update_height();
        (Some(n), gtr)
    } else {
        // we return the greater node with less = greater = None
        // and outdated height
        (n.less.take(), n)
    }
}

fn word_count(e: &Edge, mut count: usize) -> usize {
    match e {
        &None => count,
        &Some(ref n) => {
            if n.is_word { count += 1 };
            count = word_count(&n.less, count);
            count = word_count(&n.equal, count);
            word_count(&n.greater, count)
        }
    }
}

fn prefix_count(digit: u8, rest: &[u8], e: &Edge) -> usize {
    match e {
        &None => 0,
        &Some(ref n) => match digit.cmp(&n.digit) {
            Less => prefix_count(digit, rest, &n.less),
            Greater => prefix_count(digit, rest, &n.greater),
            Equal => match rest.split_first() {
                None => word_count(&n.equal, 0),
                Some((&digit, rest)) => prefix_count(digit, rest, &n.equal),
            }
        }
    }
}

fn nil_count(e: &Edge, mut count: usize) -> usize {
    match e {
        &None => count + 1,
        &Some(ref n) => {
            count = nil_count(&n.less, count);
            count = nil_count(&n.equal, count);
            nil_count(&n.greater, count)
        }
    }
}

fn average_depth_acc(e: &Edge, mut depth: usize, acc: &mut (usize, usize)) {
    match e {
        &None => {}
        &Some(ref n) => {
            if n.is_word {
                let &mut (ref mut ds, ref mut wc) = acc;
                *ds += depth;
                *wc += 1;
            }
            depth += 1;
            average_depth_acc(&n.less, depth, acc);
            average_depth_acc(&n.equal, depth, acc);
            average_depth_acc(&n.greater, depth, acc);
        }
    }
}

fn ordered_words(e: &Edge, w: &mut Vec<u8>, ws: &mut Vec<String>) {
    match e {
        &None => {}
        &Some(ref n) => {
            ordered_words(&n.less, w, ws);
            w.push(n.digit);
            if n.is_word { ws.push(String::from_utf8(w.clone()).unwrap()); }
            ordered_words(&n.equal, w, ws);
            w.pop();
            ordered_words(&n.greater, w, ws);
        }
    }
}

fn memory_usage(e: &Edge, used: &mut usize) {
    match e {
        &None => {}
        &Some(ref n) => {
            *used += mem::size_of::<Node>();
            memory_usage(&n.less, used);
            memory_usage(&n.equal, used);
            memory_usage(&n.greater, used);
        }
    }
}

fn graphviz<W: io::Write>(e: &Edge, w: &mut W, word: &mut String, color: &str) {
    match e {
        &None => {}
        &Some(ref n) => {
            word.push(n.digit as char);
            let shape_arg = if n.is_word { ", shape=doublecircle" } else { "" };
            writeln!(w, "  \"{}\" [label=\"{}\", color={}{}]",
                     word, n.digit as char, color, shape_arg).unwrap();
            if n.height > 1 {
                write!(w, "  \"{}\" -> {{", word).unwrap();
                {
                    let mut write_edge = |e: &Edge, word: &mut String| {
                        if let &Some(ref n) = e {
                            word.push(n.digit as char);
                            write!(w, " \"{}\"", word).unwrap();
                            word.pop();
                        }
                    };
                    write_edge(&n.equal, word);
                    word.pop();
                    write_edge(&n.less, word);
                    write_edge(&n.greater, word);
                }
                writeln!(w, "}}").unwrap();

                graphviz(&n.less, w, word, "blue");
                graphviz(&n.greater, w, word, "red");
                word.push(n.digit as char);
                graphviz(&n.equal, w, word, "black");
            }
            word.pop();
        }
    }
}

impl HybridTrie {
    pub fn insert_no_rebalance(&mut self, word: &str) -> bool {
        let mut inserted = false;
        match word.as_bytes().split_first() {
            None => panic!("trying to insert the empty word"),
            Some((&digit, rest)) =>
                self.root = insert_no_rebalance(digit,
                                                rest,
                                                self.root.take(),
                                                &mut inserted),
        }
        inserted
    }
}

impl Dictionnary for HybridTrie {
    fn empty() -> Self {
        HybridTrie {
            root: None,
        }
    }

    fn graphviz<P: AsRef<Path>>(&self, path: P) {
        use std::fs::File;
        use std::io::Write;
        let f = File::create(path).unwrap();
        let mut w = io::BufWriter::new(f);
        writeln!(w, "digraph hybrid {{").unwrap();
        writeln!(w, "  node [shape=circle]").unwrap();
        let mut s = String::new();
        graphviz(&self.root, &mut w, &mut s, "black");
        writeln!(w, "}}").unwrap();
    }

    fn memory_usage(&self) -> (usize, usize) {
        use std::mem;
        let mut used = mem::size_of::<Self>();
        memory_usage(&self.root, &mut used);
        (used, used)
    }

    fn insert(&mut self, word: &str) -> bool {
        let mut inserted = false;
        match word.as_bytes().split_first() {
            None => panic!("trying to insert the empty word"),
            Some((&digit, rest)) =>
                self.root = insert(digit, rest, self.root.take(), &mut inserted),
        }
        inserted
    }

    fn contains(&self, word: &str) -> bool {
        match word.as_bytes().split_first() {
            None => panic!("trying to search for the empty word"),
            Some((&digit, rest)) => contains(digit, rest, &self.root),
        }
    }

    fn remove(&mut self, word: &str) -> bool {
        let mut removed = false;
        match word.as_bytes().split_first() {
            None => panic!("trying to remove the empty word"),
            Some((&digit, rest)) =>
                self.root = remove(digit, rest, self.root.take(), &mut removed),
        }
        removed
    }

    fn word_count(&self) -> usize {
        word_count(&self.root, 0)
    }

    fn ordered_words(&self, v: &mut Vec<String>) {
        let mut word = Vec::new();
        ordered_words(&self.root, &mut word, v);
    }

    fn prefix_count(&self, prefix: &str) -> usize {
        match prefix.as_bytes().split_first() {
            None => self.word_count(),
            Some((&digit, rest)) => prefix_count(digit, rest, &self.root),
        }
    }

    fn nil_count(&self) -> usize {
        nil_count(&self.root, 0)
    }

    fn tree_height(&self) -> usize {
        height(&self.root) as usize
    }

    fn average_depth(&self) -> f32 {
        let mut acc = (0, 0);
        average_depth_acc(&self.root, 0, &mut acc);
        let (ds, wc) = acc;
        (ds as f32 / wc as f32)
    }
}

#[cfg(test)]
mod test {
    use Dictionnary;
    use super::HybridTrie;

    macro_rules! dict_tests {
        ($($test:ident),*) => {
            $(
                #[test]
                fn $test() { ::dictionnary::test::$test::<HybridTrie>() }
            )*
        }
    }

    dict_tests! {
        insert,
        remove,
        word_count,
        prefix_count,
        ordered_words
    }

    #[test]
    fn remove_reduce() {
        let mut t = HybridTrie::empty();
        let words = &["blobigotte", "blobide", "blobig", "blob", "a", "c"];
        for w in words {
            t.insert(w);
        }
        for w in words {
            assert!(t.remove(w));
            assert!(!t.contains(w));
        }
    }
}
