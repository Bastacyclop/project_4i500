use std::path::Path;

pub trait Dictionnary {
    /// Returns an empty dictionnary.
    fn empty() -> Self;
    /// Outputs a graphviz file showing the structure.
    fn graphviz<P: AsRef<Path>>(&self, path: P);
    /// Returns a (used, allocated) tuple, in bytes.
    fn memory_usage(&self) -> (usize, usize);
    /// Inserts a word in the dictionnary.
    /// Returns true if the word was not already present.
    fn insert(&mut self, word: &str) -> bool;
    /// Returns true if the dictionnary contains the word.
    fn contains(&self, word: &str) -> bool;
    /// Removes a word from the dictionnary.
    /// Returns true if the word was present.
    fn remove(&mut self, word: &str) -> bool;
    /// Returns the number of words present in the dictionnary.
    fn word_count(&self) -> usize;
    /// Extends the given vector with the words of the dictionnary,
    /// in lexicographic order.
    fn ordered_words(&self, v: &mut Vec<String>);
    /// Returns the number of words prefixed by `prefix` in the dictionnary.
    fn prefix_count(&self, prefix: &str) -> usize;
    /// Returns the number of nil pointers.
    fn nil_count(&self) -> usize;
    /// Returns the height of the tree.
    fn tree_height(&self) -> usize;
    /// Returns the average depth of the tree's leafs.
    fn average_depth(&self) -> f32;
}

#[cfg(test)]
pub mod test {
    use super::Dictionnary;
    const WORDS: &'static [&'static str] = &["let", "me", "test", "this", "man"];

    pub fn insert<D: Dictionnary>() {
        let mut dict = D::empty();
        for w in WORDS {
            assert!(dict.insert(w));
            assert!(dict.contains(w));
        }
    }

    pub fn remove<D: Dictionnary>() {
        let mut dict = D::empty();
        for w in WORDS {
            dict.insert(w);
        }
        for w in WORDS {
            assert!(dict.remove(w));
            assert!(!dict.contains(w));
        }
        assert!(!dict.remove("not present"));
    }

    pub fn word_count<D: Dictionnary>() {
        let mut dict = D::empty();
        for w in WORDS {
            dict.insert(w);
        }
        assert!(dict.word_count() == WORDS.len());
    }

    pub fn prefix_count<D: Dictionnary>() {
        let mut dict = D::empty();
        for w in WORDS {
            dict.insert(w);
        }
        assert!(dict.prefix_count("t") == 2);
        assert!(dict.prefix_count("le") == 1);
    }

    pub fn ordered_words<D: Dictionnary>() {
        let mut dict = D::empty();
        for w in WORDS {
            dict.insert(w);
        }
        let mut words = Vec::new();
        dict.ordered_words(&mut words);
        let mut sorted = WORDS.to_owned();
        sorted.sort();
        assert!(words.iter().eq(sorted.iter()));
    }
}
