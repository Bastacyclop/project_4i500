extern crate project;

use std::{io, fs};
use std::path::Path;
use project::{PatriciaTrie, HybridTrie, Dictionnary};

fn read_words<D: Dictionnary>(path: &Path) -> D {
    use std::io::BufRead;
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    let mut dict = D::empty();
    while reader.read_line(&mut line).unwrap() > 0 {
        for w in line.split_whitespace() {
            dict.insert(w);
        }
        line.clear();
    }

    dict
}

fn main() {
    let pat_path = &Path::new("shakespeare/julius_caesar.txt");
    let hyb_path = &Path::new("shakespeare/measure.txt");

    let mut pat: PatriciaTrie = read_words(pat_path);
    let hyb: HybridTrie = read_words(hyb_path);

    let pat_conv = HybridTrie::from(&mut pat);
    let mut hyb_conv = PatriciaTrie::from(&hyb);

    let mut pat_words = &mut Vec::new();
    pat.ordered_words(pat_words);
    for w in pat_words.iter() { assert!(pat_conv.contains(w)); }
    assert_eq!(pat_words.len(), pat_conv.word_count());
    println!("conversion from patricia to hybrid successful: {:?}", pat_path);

    let mut hyb_words = &mut Vec::new();
    hyb.ordered_words(hyb_words);
    for w in hyb_words.iter() { assert!(hyb_conv.contains(w)); }
    assert_eq!(hyb_words.len(), hyb_conv.word_count());
    println!("conversion from hybrid to patricia successful: {:?}", hyb_path);

    let merge = PatriciaTrie::merge(&mut pat, &mut hyb_conv);
    for w in pat_words.iter().chain(hyb_words.iter()) {
        assert!(merge.contains(w));
    }
    println!("patricias merge successful");
}
