extern crate project;
extern crate time;

use std::{fs, io};
use std::path::Path;
use project::{Dictionnary, HybridTrie};

fn measure<F: FnOnce()>(f: F) -> u64 {
    let marker = time::precise_time_ns();
    f();
    time::precise_time_ns() - marker
}

fn process<F: FnMut(&str)>(path: &Path, mut f: F) {
    use std::io::BufRead;
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    while reader.read_line(&mut line).unwrap() > 0 {
        for w in line.split_whitespace() {
            f(w);
        }
        line.clear();
    }
}

fn main() {
    let mut t = HybridTrie::empty();
    let bt = measure(|| {
        for entry in fs::read_dir("shakespeare").unwrap() {
        let path = entry.unwrap().path();
            if path.is_file() {
                process(&path, |w| { t.insert(w); });
            }
        }
    });

    let mut t_no_rebalance = HybridTrie::empty();
    let bt_no_rebalance = measure(|| {
        for entry in fs::read_dir("shakespeare").unwrap() {
            let path = entry.unwrap().path();
            if path.is_file() {
                process(&path, |w| { t_no_rebalance.insert_no_rebalance(w); });
            }
        }
    });


    let print_stuff = |t: HybridTrie, bt| {
        print!("build time: ");
        print_time(bt);
        println!("");
        print!("word count: ");
        print_unitless(t.word_count());
        println!("");
        println!("height: {}", t.tree_height());
        println!("average depth: {}", t.average_depth());
    };

    println!("rebalancing insertion:");
    print_stuff(t, bt);
    println!("not rebalancing insertion:");
    print_stuff(t_no_rebalance, bt_no_rebalance);
}

macro_rules! print_div {
    ($num:ident, $units:expr) => {{
        let mut n = $num;
        for unit in &$units {
            if n < 100_000 {
                print!("{} {}", n, unit);
                return;
            }
            n /= 1_000;
        }
        panic!("no suitable unit");
    }}
}

fn print_time(ns: u64) {
    print_div!(ns, ["ns", "µs", "ms", " s"]);
}

fn print_unitless(value: usize) {
    print_div!(value, [" ", "k", "M", "G"]);
}
