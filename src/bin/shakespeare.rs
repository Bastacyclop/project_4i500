extern crate project;
extern crate time;

use std::{fs, io};
use std::path::Path;
use project::{Dictionnary, HybridTrie, PatriciaTrie};

#[derive(Debug)]
struct Result {
    build: u64,
    insert: u64,
    suppress: u64,
    words: usize,
    nils: usize,
    height: usize,
    average_depth: f32,
    memory: (usize, usize),
}

fn measure<F: FnOnce()>(f: F) -> u64 {
    let marker = time::precise_time_ns();
    f();
    time::precise_time_ns() - marker
}

fn process<D: Dictionnary>(path: &Path) -> Result {
    use std::io::BufRead;
    let f = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(f);

    let mut dict = D::empty();
    let btime = measure(|| {
        while reader.read_line(&mut line).unwrap() > 0 {
            for w in line.split_whitespace() {
                dict.insert(w);
            }
            line.clear();
        }
    });
    let wc = dict.word_count();
    let nc = dict.nil_count();
    let h = dict.tree_height();
    let d = dict.average_depth();
    let m = dict.memory_usage();

    let additionnal = ["bonjour", "j'aime", "les", "patates", "violettes"];
    let itime = measure(|| for w in &additionnal { dict.insert(w); });
    let stime = measure(|| for w in &additionnal { dict.remove(w); });

    Result {
        build: btime,
        insert: itime / additionnal.len() as u64,
        suppress: stime / additionnal.len() as u64,
        words: wc,
        nils: nc,
        height: h,
        average_depth: d,
        memory: m,
    }
}

fn main() {
    let mut results = Vec::new();
    for entry in fs::read_dir("shakespeare").unwrap() {
        let path = entry.unwrap().path();
        if path.is_file() {
            println!("processing {:?}", &path);
            let htr = process::<HybridTrie>(&path);
            let ptr = process::<PatriciaTrie>(&path);
            results.push((path, htr, ptr));
        }
    }

    let width = 22+3+8+3+8+3+8+3+3+3+3+3+7+3+7+3+8+3+8;
    let print_separator = || {
        for _ in 0..width { print!("-"); }
        println!("");
    };
    let print_header = |name| {
        print_separator();
        println!("{:^w$}", name, w = width);
        print_separator();
        println!("{:^22} | {:^8} | {:^8} | {:^8} | {:^3} | {:^3} | {:^7} | {:^7} | {:^8} | {:^8}",
                 "path", "build", "insert", "remove", "h", "d", "words", "nils", "used", "alloc");
        print_separator();
    };
    let print_result = |f: &Path, r: &Result| {
        print!("{:<22} | ", f.file_stem().unwrap().to_str().unwrap());
        print_time(r.build);
        print!(" | ");
        print_time(r.insert);
        print!(" | ");
        print_time(r.suppress);
        print!(" | {:>3} | {:>3} | ", r.height, r.average_depth);
        print_unitless(r.words);
        print!(" | ");
        print_unitless(r.nils);
        print!(" | ");
        print_memory(r.memory.0);
        print!(" | ");
        print_memory(r.memory.1);
        println!("");
    };

    print_header("Hybrid Trie");
    for &(ref file, ref htr, _) in &results { print_result(file, htr) }

    print_header("Patricia Trie");
    for &(ref file, _, ref ptr) in &results { print_result(file, ptr) }

    print_separator();
    println!("build: time to build all the structure");
    println!("insert: average insert time for a few words");
    println!("remove: average suppression time for a few words");
    println!("h: tree height");
    println!("d: average word depth");
    print_separator();
}

macro_rules! print_div {
    ($num:ident, $units:expr) => {{
        let mut n = $num;
        for unit in &$units {
            if n < 100_000 {
                print!("{:>5} {}", n, unit);
                return;
            }
            n /= 1_000;
        }
        panic!("no suitable unit");
    }}
}

fn print_time(ns: u64) {
    print_div!(ns, ["ns", "µs", "ms", " s"]);
}

fn print_memory(bytes: usize) {
    print_div!(bytes, [" o", "ko", "Mo", "Go"]);
}

fn print_unitless(value: usize) {
    print_div!(value, [" ", "k", "M", "G"]);
}
