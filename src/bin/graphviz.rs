extern crate project;

pub const PHRASE: &'static str = "A quel genial professeur de dactylographie sommes nous redevables de la superbe phrase ci dessous, un modele du genre, que tout dactylo connait par coeur puisque elle fait appel a chacune des touches du clavier de la machine a ecrire ?";

use std::{fs, io, env};
use std::io::prelude::*;
use std::path::Path;
use std::process::Command;
use project::{Dictionnary, HybridTrie, PatriciaTrie};

fn graphviz<D: Dictionnary>(name: &str, s: &str) {
    let mut gv_output = Path::new("graphviz/").to_path_buf();
    gv_output.push(name);
    gv_output.set_extension("gv");

    let mut d = D::empty();
    if let Ok(f) = fs::File::open(s) {
        println!("processing file: '{}'", s);
        let mut line = String::new();
        let mut reader = io::BufReader::new(f);
        while reader.read_line(&mut line).unwrap() > 0 {
            for word in line.split_whitespace() {
                d.insert(word);
            }
            line.clear();
        }
    } else {
        println!("processing words (not file): '{}'", s);
        for word in s.split_whitespace() {
            d.insert(word);
        }
    }

    d.graphviz(&gv_output);

    let mut png_output = gv_output.clone();
    png_output.set_extension("png");
    let status = Command::new("dot")
        .arg("-Tpng")
        .arg("-o")
        .arg(png_output)
        .arg(gv_output)
        .status()
        .unwrap();
    if !status.success() {
        println!("could not generate png output");
    }
}


fn main() {
    let mut args = env::args();
    args.next();
    if let Some(path) = args.next() {
        graphviz::<HybridTrie>("hybrid", &path);
        graphviz::<PatriciaTrie>("patricia", &path);
    } else {
        graphviz::<HybridTrie>("hybrid", PHRASE);
        graphviz::<PatriciaTrie>("patricia", PHRASE);
    }
}
