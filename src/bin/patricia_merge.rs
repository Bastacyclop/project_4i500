extern crate project;
extern crate time;

use std::{fs, io, cmp};
use std::path::Path;
use project::{Dictionnary, PatriciaTrie};

fn read_words<F: FnMut(&str)>(path: &Path, mut f: F) {
    use std::io::BufRead;
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    while reader.read_line(&mut line).unwrap() > 0 {
        for w in line.split_whitespace() {
            f(w);
        }
        line.clear();
    }
}

fn measure<F: FnOnce() -> R, R>(f: F) -> (u64, R) {
    let marker = time::precise_time_ns();
    let r = f();
    (time::precise_time_ns() - marker, r)
}

fn print_time(ns: u64) {
    let mut n = ns;
    for unit in &["ns", "µs", "ms", "s "] {
        if n < 100_000 {
            print!("{:>5} {}", n, unit);
            return;
        }
        n /= 1_000;
    }
    panic!("no suitable unit!");
}

fn main() {
    let a_path = &Path::new("shakespeare/hamlet.txt");
    let b_path = &Path::new("shakespeare/romeo_juliet.txt");

    let mut a = PatriciaTrie::empty();
    let (a_time, _) = measure(|| {
        read_words(a_path, |w| {
            a.insert(w);
        });
    });

    let mut b = PatriciaTrie::empty();
    let (b_time, _) = measure(|| {
        read_words(b_path, |w| {
            b.insert(w);
        });
    });

    let mut with_insert = PatriciaTrie::empty();
    let (insert_time, _) = measure(|| {
        read_words(a_path, |w| {
            with_insert.insert(w);
        });
        read_words(b_path, |w| {
            with_insert.insert(w);
        });
    });

    let (merge_time, with_merge) = measure(|| {
        PatriciaTrie::merge(&mut a, &mut b)
    });

    assert_eq!(with_insert.word_count(), with_merge.word_count());
    let seq_time = insert_time;
    let par_time = cmp::max(a_time, b_time) + merge_time;

    let print_time = |name, time| {
        print!("{:>18} time: ", name);
        print_time(time);
        println!("");
    };

    print_time(a_path.file_stem().unwrap().to_str().unwrap(), a_time);
    print_time(b_path.file_stem().unwrap().to_str().unwrap(), b_time);

    print_time("merge", merge_time);
    print_time("sequential", seq_time);
    print_time("parallel", par_time);
}
